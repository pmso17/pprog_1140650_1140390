/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.controller;

import java.util.ArrayList;
import tpg.pprog.model.Empresa;
import tpg.pprog.model.ListaAutor;
import tpg.pprog.model.ListaUtilizador;
import tpg.pprog.model.Submissiveis;

/**
 *
 * @author N550JK-CN456H
 */
public class SubmeterArtigoController {

    private ListaAutor la;
    private ListaUtilizador lu;

    /**
     * obtem a lista de submissiveis
     * @return lista de submissiveis
     */
    public ArrayList<Submissiveis> getSubmissiveis() {
        return new Empresa().getSubmissiveis();
    }

    /**
     * obtem a lista de autores
     *
     * @return la
     */
    public ListaAutor getListaAutor() {
        return la;
    }

    /**
     * obtem a lista de utilizadores
     *
     * @return lu
     */
    public ListaUtilizador getListaUtilizador() {
        return new ListaUtilizador();
    }

    
}
