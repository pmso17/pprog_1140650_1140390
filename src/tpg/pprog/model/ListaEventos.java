/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.awt.List;
import java.io.Serializable;
import java.util.ArrayList;
import tpg.utils.InputOutputObjetos;

/**
 *
 * @author N550JK-CN456H
 */
public class ListaEventos implements Submissiveis, Serializable {
    private ArrayList<Evento> listaEventos;

    /**
     * Construtor Vazio
     * Ele vai buscar os dados dos eventos a um ficheiro binário
     */
    public ListaEventos() {
        //Como não é para implementar criar eventos aqui está uma lista predefinida de eventos em forma binária guardada com o nome ListaEventos_0.bin
        this((ListaEventos)new InputOutputObjetos().lerObjeto("ListaEventos_0.bin"));
    }

    public ListaEventos(ArrayList listaEventos) {
        this.listaEventos = listaEventos;
    }
    
    public ListaEventos(ListaEventos outraLista){
        this(outraLista.listaEventos);
    }
    
    /**
     * Instanciação Automática de uma lista de eventos pré-definida
     * 
     * @param listaEventos serve apenas para distinguir dos restantes construtores
     */
    public ListaEventos(String listaEventos) {
        this.listaEventos= new ArrayList<Evento>();
        this.listaEventos.add(new Evento("Comic Con", "Uma boa festa onde vê se coisas muito fixes", "22/11/10", "30/1/22", "Rua Falsa 123"));
        this.listaEventos.add(new Evento("Festa de PPROG", "Queres um copo de JAVA?", "10/10/10", "11/11/11", "Rua String Integer"));
        this.listaEventos.add(new Evento("Festa DE ESOFT", "Não faças caso deste uso", "22/11/10", "30/1/22", "Rua S.T.U.P.I.D FURPS",new ListaSessaoTematica(1, "Melhor Sessao Tematica de Sempre")));
    }

    /**
     * 
     * @param i posição
     * @return evento da posição i
     */
    public Evento getEvento(int i) {
        return listaEventos.get(i);
    }
    
    /**
     * 
     * @return lista de eventos
     */
    public ArrayList getListaEventos() {
        return listaEventos;
    }
    
    
}
