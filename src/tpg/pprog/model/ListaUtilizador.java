/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;
import java.util.ArrayList;
import tpg.utils.InputOutputObjetos;

/**
 *
 * @author N550JK-CN456H
 */
public class ListaUtilizador implements Serializable {

    private ArrayList<Utilizador> lu;

    /**
     * Construtor Vazio
     */
    public ListaUtilizador() {
        this(((ListaUtilizador) new InputOutputObjetos().lerObjeto("ListaUtilizador_0.bin")).lu);
    }

    /**
     * Construtor com Parametros
     * @param lu lista de utilizadores
     */
    public ListaUtilizador(ArrayList<Utilizador> lu) {
        this.lu = lu;
    }

    /**
     * adiciona o utilizador
     *
     * @param u utilizador a ser adicionado
     */
    public void add(Utilizador u) {
        lu.add(u);
    }

    /**
     * procura um utilizador utilizando o seu email
     *
     * @param email do utilizador
     * @return utilizador
     */
    public Utilizador findUtilizador(String email) {
        for (Utilizador u : this.lu) {
            if (u.getEmail().equals(email)) {
                return u;
            }
        }
        return null;
    }

    /**
     * guarda a lista de utilizadores num ficheiro .bin
     */
    public void guardar() {
        new InputOutputObjetos().gravarObjeto(this, "ListaUtilizador_0.bin");
    }
}
