/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author N550JK-CN456H
 */
public class ListaAutor implements Serializable{
    private List<Autor> listaAutores;

    /**
     * Construtor Vazio
     */
    public ListaAutor() {
        listaAutores= new ArrayList<>();
    }

/**
 * Construtor Com Parametro
 * @param listaAutores lista de autores
 */
    public ListaAutor(List<Autor> listaAutores) {
        this.listaAutores = listaAutores;
    }
    
/**
 * obtem o autor da posição i
 * @param indice é a posição em que o autor está na lista
 * @return autor
 */
    public Autor obterAutor(int indice) {
        return listaAutores.get(indice);
    }

    /**
     * adiciona o autor da lista
     * @param autor a ser adicionado
     * @return boolean
     */
    public boolean adicionarAutor(Autor autor) {
        if (!this.listaAutores.contains(autor)) {
            return this.listaAutores.add(autor);
        }
        return false;
    }
/**
     * remove o autor da lista
     * @param autor a ser removido
     * @return boolean
     */
    public boolean removerAutor(Autor autor) {
        return this.listaAutores.remove(autor);
    }

    /**
     * 
     * @return tamanho da lista de autores
     */
    public int tamanho() {
        return this.listaAutores.size();
    }

    /**
     * 
     * @param autor a ser encontrado
     * @return posição
     */
    public int indiceDe(Autor autor) {
        return this.listaAutores.indexOf(autor);
    }
    
    /**
     * verifica se existe esse autor
     * @param autor a ser verificado
     * @return um boolean
     */
    public boolean contem(Autor autor){
        return this.listaAutores.contains(autor);
    }

    /**
     * 
     * @return lista de autores
     */
    public List<Autor> getLista() {
        return listaAutores;
    }
    
    
/**
 * 
 * @return string com os dados da lista
 */
    @Override
    public String toString() {
        String s =  "[";
        System.out.println(listaAutores.size());
        for (Autor a: listaAutores) {
            s=s+a.getNome()+", ";
        }
        s+="]";
        System.out.println(s);
        return s;
    }
    
    
    
}
