/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class SessaoTematica implements Submissiveis, Serializable{
    private int codigo;
    private String descricao;
    private ListaProponentes listaProponente;

    /**
     * Construtor com todos os parametros
     * 
     * @param codigo da sessao tematica
     * @param descricao da sessao tematica
     * @param listaProponente da sessao tematica
     */
    public SessaoTematica(int codigo, String descricao, ListaProponentes listaProponente) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.listaProponente = new ListaProponentes();
    }


    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @return the listaProponente
     */
    public ListaProponentes getListaProponente() {
        return listaProponente;
    }

    /**
     * cria uma string com os dados da sessão tematica
     * @return string
     */
    @Override
    public String toString() {
        return "   -"+getDescricao();
    }
}
