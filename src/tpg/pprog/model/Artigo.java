/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.File;
import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class Artigo implements Serializable{
    private String titulo;
    private String resumo;
    private ListaAutor lAut;
    private File pdf;
    private Utilizador autorCorrespondente;

    /**
     * Construtor com Parametros
     * @param titulo do artigo
     * @param resumo do artigo
     * @param lAut do artigo
     * @param pdf do artigo
     * @param autorCorrespondente do artigo
     */
    public Artigo(String titulo, String resumo, ListaAutor lAut, File pdf, Utilizador autorCorrespondente) {
        this.titulo = titulo;
        this.resumo = resumo;
        this.lAut = lAut;
        this.pdf = pdf;
        this.autorCorrespondente = autorCorrespondente;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the resumo
     */
    public String getResumo() {
        return resumo;
    }

    /**
     * @param resumo the resumo to set
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    /**
     * @return the lAut
     */
    public ListaAutor getlAut() {
        return lAut;
    }

    /**
     * @param lAut the lAut to set
     */
    public void setlAut(ListaAutor lAut) {
        this.lAut = lAut;
    }

    /**
     * @return the pdf
     */
    public File getPdf() {
        return pdf;
    }

    /**
     * @param pdf the pdf to set
     */
    public void setPdf(File pdf) {
        this.pdf = pdf;
    }

    /**
     * @return the autorCorrespondente
     */
    public Utilizador getAutorCorrespondente() {
        return autorCorrespondente;
    }

    /**
     * @param autorCorrespondente the AutorCorrespondente to set
     */
    public void setAutorCorrespondente(Utilizador autorCorrespondente) {
        this.autorCorrespondente = autorCorrespondente;
    }

    /**
     * @return string com os dados do artigo
     */
    @Override
    public String toString() {
        return titulo+" | "+resumo+" | "+lAut+" | "+pdf.getName();
    }
    
    
    
}
