package tpg.pprog.model;

import tpg.pprog.model.Autor;
import tpg.pprog.model.ListaAutor;
import javax.swing.AbstractListModel;

public class ModeloListaAutores extends AbstractListModel {

    private ListaAutor listaAutores;
    /**
     * Construtor com parametros
     * @param listaAutores lista de autores
     */
    public ModeloListaAutores(ListaAutor listaAutores) {
        this.listaAutores = listaAutores;
    }

    /**
     * retorna o tamalho da lista
     * @return tamalho da lista
     */
    @Override
    public int getSize() {
        return this.listaAutores.tamanho();
    }

    /**
     * retorna o elemento na posição indice
     * @param indice do elemento
     * @return element 
     */
    @Override
    public Object getElementAt(int indice) {
        return this.listaAutores.obterAutor(indice);
    }
    
    /**
     * adiciona o autor à lista
     * @param autor a ser adicionado
     * @return boolean
     */
    public boolean addElement(Autor autor){
        boolean autorAdicionado = this.listaAutores.adicionarAutor(autor);
        if(autorAdicionado)
            fireIntervalAdded(autor, this.getSize()-1, this.getSize()-1);
        return autorAdicionado;
    } 
/**
     * remove o autor à lista
     * @param autor a ser removido
     * @return boolean
     */
    public boolean removeElement(Autor autor){
        int indice = this.listaAutores.indiceDe(autor);
        boolean jogadorRemovido = this.listaAutores.removerAutor(autor);
        if(jogadorRemovido)
            fireIntervalRemoved(autor, indice, indice);
        return jogadorRemovido;
    }
    /**
     * verifica se existe um autor, se existir retorna true
     * @param autor a ser verificado
     * @return boolean
     */
    public boolean contains(Autor autor){
        return this.listaAutores.contem(autor);
    }
    
}
