/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class Evento implements Serializable,Submissiveis {
    private String titulo;
    private String descricao;
    private String dataInicio;
    private String dataFim;
    private Local local;
    private ListaSessaoTematica sessoesTematicas;

    /**
     * Construtor de Evento com Parametros sem Sessões Temáticas
     * @param titulo do evento
     * @param descricao do evento
     * @param dataInicio do evento
     * @param dataFim do evento
     * @param local do evento
     */
    public Evento(String titulo, String descricao, String dataInicio, String dataFim, String local) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.local = new Local(local);
        this.sessoesTematicas=null;
    }
    /**
     * Construtor de Evento com Parametros com Sessões Temáticas
     * @param titulo do evento
     * @param descricao do evento
     * @param dataInicio do evento
     * @param dataFim do evento
     * @param local do evento
     * @param sessaoTematica  do evento
     */
    public Evento(String titulo, String descricao, String dataInicio, String dataFim, String local,ListaSessaoTematica sessaoTematica) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.local = new Local(local);
        this.sessoesTematicas=sessaoTematica;
    }


    /**
     * @return lista de sessões tematicas
     */
    public ListaSessaoTematica getSessoesTematicas() {
        return sessoesTematicas;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @return the dataInicio
     */
    public String getDataInicio() {
        return dataInicio;
    }

    /**
     * @return the dataFim
     */
    public String getDataFim() {
        return dataFim;
    }

    /**
     * @return the local
     */
    public Local getLocal() {
        return local;
    }
    /**
     * @return string com o titulo do evento
     */
    @Override
    public String toString() {
        return this.getTitulo();
    }

    
}
