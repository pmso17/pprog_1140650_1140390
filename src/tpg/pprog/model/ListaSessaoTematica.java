/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author N550JK-CN456H
 */
public class ListaSessaoTematica implements Submissiveis, Serializable{
    private ArrayList<SessaoTematica> listaSessaoTematica;

    /**
     * Construtor com Parametros
     * @param codigo da sessão tematica
     * @param descricao da sessao tematica
     */
    public ListaSessaoTematica(int codigo,String descricao) {
        this.listaSessaoTematica = new ArrayList<SessaoTematica>();
        this.listaSessaoTematica.add(new SessaoTematica(codigo, descricao, new ListaProponentes()));
        this.listaSessaoTematica.add(new SessaoTematica(0, "Sessao sobre Teoria Quantica", new ListaProponentes()));
    }

    /**
     * 
     * @return lST lista de sessões temáticas do evento
     */
    public ArrayList getListaSessaoTematica() {
        return listaSessaoTematica;
    }
    
    /**
     * 
     * @param i posição do evento
     * @return st sessão temática da posição i
     */
    public SessaoTematica getSessaoTematica(int i) {
        return listaSessaoTematica.get(i);
    }
}
