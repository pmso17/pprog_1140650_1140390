/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author N550JK-CN456H
 */
class ListaProponentes implements Serializable {
    private ArrayList<Proponentes> listProponentes;

    /**
     * Construtor Default
     * 
     * Cria uma lista de proponentes pré-definida
     * Não implementei a funcionalidade de personalizar a lista (adicionar e retirar proponentes) pois só se utiliza os proponentes para a criação de sessões temáticas
     */
    public ListaProponentes() {
        listProponentes= new ArrayList<>();
        listProponentes.add(new Proponentes("Pedro Oliveira","pmso@pmso.com","password"));
        listProponentes.add(new Proponentes("Josefino","josefino@bug.com","password"));
        listProponentes.add(new Proponentes("António Martins","antonio@coolmail.com","password"));
        listProponentes.add(new Proponentes("Son Goku","super@sayan.two","password"));
    }

    
}
