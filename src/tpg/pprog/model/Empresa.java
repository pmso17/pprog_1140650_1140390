/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.util.ArrayList;
import tpg.utils.InputOutputObjetos;

/**
 *
 * @author N550JK-CN456H
 */
public class Empresa {

    private ListaEventos le;

    /**
     * Construtor Vazio
     */
    public Empresa() {
        le = new ListaEventos();
    }

    /**
     * obtem a lista de submissiveis
     *
     * @return ArrayList
     */
    public ArrayList getSubmissiveis() {
        ArrayList<Submissiveis> submissiveis = null;

        submissiveis = new ArrayList<>(le.getListaEventos());
        for (int i = 0; i < le.getListaEventos().size(); i++) {
            Evento e = ((Evento) le.getEvento(i));
            ListaSessaoTematica listaSessaoTematica = e.getSessoesTematicas();
            if (listaSessaoTematica != null) {
                for (SessaoTematica st : (ArrayList<SessaoTematica>) listaSessaoTematica.getListaSessaoTematica()) {
                    submissiveis.add(st);
                }
            }

        }
        new InputOutputObjetos().gravarObjeto(le, "ListaEventos_0.bin");
        return submissiveis;
    }
}
