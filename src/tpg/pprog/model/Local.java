/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class Local implements Serializable {

    private String morada;

    /**
     * Construtor Com parametro
     *
     * @param morada do local
     */
    public Local(String morada) {
        this.morada = morada;
    }

    /**
     * @return morada
     */
    public String getMorada() {
        return morada;
    }

    /**
     * @param morada to set
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

}
