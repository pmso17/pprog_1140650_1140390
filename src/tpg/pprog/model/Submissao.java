/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class Submissao implements Serializable{
    private Artigo a;
    private Submissiveis s;

    /**
     * Construtor com parametros
     * @param a artigo da submissao
     * @param s onde é submetido o artigo
     */
    public Submissao(Artigo a,Submissiveis s) {
        this.a = a;
        this.s=s;
    }

    /**
     * Construtor Vazio
     */
    public Submissao() {
    this(new Artigo("sem titulo", "sem descrição", null, null,null),new Evento(null, null, null, null, null));
    }
    
    
    /**
     * 
     * @return a artigo
     */
    public Artigo getArtigo(){
        return a;
    }

    /**
     * 
     * @return s onde o artigo é submetido
     */
    public Submissiveis getS() {
        return s;
    }

    /**
     * forma uma string com as informações da submissao
     * @return string
     */
    @Override
    public String toString() {
        return a+s.toString();
    }
    
    
}
