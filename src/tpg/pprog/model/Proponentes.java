/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class Proponentes extends Utilizador implements Serializable {

    /**
     * Construtor com Parametros
     *
     * @param nome do proponente
     * @param email do proponente
     * @param password do proponente
     */
    public Proponentes(String nome, String email, String password) {
        super(nome, email, password);
    }

}
