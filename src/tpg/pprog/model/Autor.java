/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class Autor implements Serializable{
    private String nome;
    private String afiliacao;
    private String email;

    /**
     * Construtor com Parametros
     * @param nome do autor
     * @param afiliacao do autor
     * @param email do autor
     */
    public Autor(String nome, String afiliacao, String email) {
        setNome(nome);
        setAfiliacao(afiliacao);
        setEmail(email);
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the afiliacao
     */
    public String getAfiliacao() {
        return afiliacao;
    }

    /**
     * @param afiliacao the afiliacao to set
     */
    public void setAfiliacao(String afiliacao) {
        this.afiliacao = afiliacao;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return string com os dados do autor
     */
    @Override
    public String toString() {
        return this.nome+"|"+this.afiliacao+"|"+this.email;
    }
    
    
    
}
