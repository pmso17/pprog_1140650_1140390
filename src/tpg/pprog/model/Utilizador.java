/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.model;

import java.io.Serializable;

/**
 *
 * @author N550JK-CN456H
 */
public class Utilizador implements Serializable{
    private String nome;
    private String email;
    private String password;

    /**
     * Construtor com parametros
     * @param nome do utilizador
     * @param email do utilizador
     * @param password  do utilizador
     */
    public Utilizador(String nome, String email,String password) {
        this.nome = nome;
        this.email = email;
    }

    /**
     * retorna o nome
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * retorna o email do utilizador
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * retorna a password do utilizador
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }


    
}
