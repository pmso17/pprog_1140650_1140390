/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.UI;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import tpg.pprog.UI.*;
import tpg.pprog.model.Artigo;
import tpg.pprog.model.Autor;
import tpg.pprog.model.Evento;
import tpg.pprog.model.ListaAutor;
import tpg.pprog.model.ListaEventos;
import tpg.pprog.model.Submissao;
import tpg.pprog.model.Utilizador;
import tpg.utils.InputOutputObjetos;

/**
 *
 * @author N550JK-CN456H
 */
public class TPGPPROG {

    /**
     * Método Main
     * Ele inicia a interface gráfica Janela
     * @param args argumentos que se podem colocar ao iniciar o programa (não usado)
     */
    public static void main(String[] args) {
        new Janela();
        
        /**
         * Código de teste de funcionalidades:
         */
//        
//            new InputOutputObjetos().gravarObjeto(new ListaEventos("mama"),"ListaEventos_0.bin");
//        Evento e = new Evento("Comic Con", "Desc", "22/11/12", "11/11/11", "123 Fake Street");
//        
//        System.out.println(e);
//
//        List<Autor> la = new ArrayList<>();
//        ListaAutor lA = new ListaAutor(la);
//        lA.adicionarAutor(new Autor("Gregorio", "ISEP", "gregorio@chamo.com"));
//        lA.adicionarAutor(new Autor("Josefino", "ISEP", "123@123.com"));
//        lA.adicionarAutor(new Autor("Pedro", "ISUP", "123@123.com"));
//        
//
//        Submissao s = new Submissao(new Artigo("IUPIs", "São fixes",lA, new File(""), new Utilizador("Gregorio", "gregorio@chamo.com", "tareco")), e);
//        new InputOutputObjetos().gravarObjeto(s, "Porque.bin");
//        System.out.println(s);
//        Submissao s1=(Submissao)new InputOutputObjetos().lerObjeto("Submissao.bin");
//        System.out.println(s1);

    }

}
