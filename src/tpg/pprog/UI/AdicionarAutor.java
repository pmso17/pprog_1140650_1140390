/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.UI;

import tpg.pprog.model.ModeloListaAutores;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import tpg.pprog.model.Autor;
import tpg.pprog.model.ListaAutor;

/**
 *
 * @author N550JK-CN456H
 */
public class AdicionarAutor extends JDialog {

    private JDialog framePai;
    private JList listaAutor;
    private ListaAutor la;
    private JTextField txtNome, txtAlifiacao, txtEmail;

    /**
     * Construtor
     * 
     * Cria um dialogo para adicionar os dados de um autor
     * @param framePai donde o frame é amostrado 
     * @param listaAutor Jlist dos autores
     * @param la lista de autores
     */
    public AdicionarAutor(JDialog framePai, JList listaAutor,ListaAutor la) {
        super(framePai, "Adicionar Autor", true);

        this.framePai = framePai;
        this.listaAutor = listaAutor;
        this.la= la;

        this.setLayout(new BorderLayout());

        JLabel label = new JLabel("Adicionar Autor");
        label.setFont(new Font(Font.SERIF, Font.BOLD, 40));
        this.add(label, BorderLayout.NORTH);

        this.add(criarPanelDadosAutor(), BorderLayout.CENTER);

        this.add(criarButoes(), BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(framePai);
        this.setVisible(true);
    }

    /**
     * cria o painel com os campos para preencher os dados do autor
     * @return p
     */
    private JPanel criarPanelDadosAutor() {
        JPanel p = new JPanel(new GridLayout(3, 2));

        p.add(new JLabel("Nome:"));

        txtNome = new JTextField();
        p.add(txtNome);

        p.add(new JLabel("Afiliação:"));

        txtAlifiacao = new JTextField();
        p.add(txtAlifiacao);

        p.add(new JLabel("E-mail:"));
        txtEmail = new JTextField();
        p.add(txtEmail);

        return p;
    }

    /**
     * cria um painel com botao que adiciona o novo autor à lista de autores
     * @return 
     */
    private JPanel criarButoes() {
        JPanel p = new JPanel();
        JButton botaoOK = new JButton("OK");
        botaoOK.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Autor autor = new Autor(txtNome.getText(), txtAlifiacao.getText(), txtEmail.getText());
                    ModeloListaAutores modeloListaAutores
                            = (ModeloListaAutores) listaAutor.getModel();

                    boolean autorAdicionado
                            = modeloListaAutores.addElement(autor);
                    la.adicionarAutor(autor);
                    System.out.println("IM HERE AUTOR");
                    dispose();
                    if (!autorAdicionado) {
                        JOptionPane.showMessageDialog(
                                framePai,
                                "Autor já existente!",
                                "Novo Autor",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(
                            framePai,
                            ex.getMessage(),
                            "Novo Autor",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
        });

        JButton botaoCancelar = new JButton("Cancelar");
        botaoCancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        p.add(botaoOK);
        p.add(botaoCancelar);
        return p;
    }

}
