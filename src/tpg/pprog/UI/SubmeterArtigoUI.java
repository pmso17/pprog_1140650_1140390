/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.UI;

import tpg.pprog.model.ModeloListaAutores;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import tpg.pprog.controller.SubmeterArtigoController;
import tpg.pprog.model.*;
import tpg.utils.CodificadorDescodificadorSubmissaoTxt;
import tpg.utils.InputOutputObjetos;

/**
 *
 * @author N550JK-CN456H
 */
public class SubmeterArtigoUI extends JDialog {

    private JFrame framePai;
    private SubmeterArtigoController controller;
    private ListaUtilizador lu;
    private ListaAutor la;

    private JComboBox cmbSubmessiveis;
    private JTextField txtTitulo;
    private JTextArea textAreaResumo;
    private JTextField labelPDF;
    private JList list;

    /**
     * Construtor
     *
     * cria uma interface gráfica especifica para submeter artigos, podendo
     * posteriormente visualizar as submissões
     *
     * a interface é composta por 3 componentes principais (2 panels e 1 label)
     *
     * @param framePai neste caso será sempre um objeto do tipo Janela (que
     * extende JFrame)
     */
    public SubmeterArtigoUI(Janela framePai) {
        super(framePai, "Submeter Artigo", true);

        controller = new SubmeterArtigoController();

        this.framePai = framePai;
        this.setLayout(new BorderLayout());
        this.la = controller.getListaAutor();
        this.lu = controller.getListaUtilizador();

        this.add(criarLabelTituloFormulario(), BorderLayout.NORTH);
        this.add(criarPanelFormulario(), BorderLayout.CENTER);
        this.add(criarPanelButoesFormulario(), BorderLayout.SOUTH);

        setIcon();

        setSize(700, 900);
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);

    }

    /**
     * altera o icone default da janela do java para a imagem contida no
     * ficheiro imagem.png
     */
    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("logo_Artigo.png")));
    }

    /**
     * cria a label que fica no topo
     *
     * @return lbl
     */
    private JLabel criarLabelTituloFormulario() {
        JLabel lbl = new JLabel("Submeter Artigo");
        lbl.setFont(new Font(Font.SERIF, Font.BOLD, 40));
        return lbl;
    }

    /**
     * cria painel com os campos que o utilizador preenche (submissivel, titulo,
     * resumo, autores e ficheiro pdf)
     *
     * @return p
     */
    private JPanel criarPanelFormulario() {
        JPanel p = new JPanel(new GridLayout(2, 1));
        p.add(criarPainelSubmemessivelTituloResumo());
        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(criarPainelAutores(), BorderLayout.CENTER);
        p1.add(criarPainelFicheiros(), BorderLayout.SOUTH);
        p.add(p1);
        return p;
    }

    /**
     * cria painel com os campos simples (campos de seleção e de preenchimento)
     *
     * @return p
     */
    private JPanel criarPainelSubmemessivelTituloResumo() {
        JPanel p = new JPanel(new GridLayout(3, 2, 0, 10));
        p.setBorder(new EmptyBorder(10, 0, 10, 0));
        txtTitulo = new JTextField();
        p.add(new JLabel("Submeter Artigo a:"));
        p.add(criarComboBoxSubmessiveis());
        p.add(new JLabel("Titulo:"));
        p.add(txtTitulo);
        p.add(new JLabel("Resumo:"));
        textAreaResumo = new JTextArea(20, 5);
        textAreaResumo.setLineWrap(true);
        textAreaResumo.setWrapStyleWord(true);
        JScrollPane scroll = new JScrollPane(textAreaResumo);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        p.add(scroll);
        return p;
    }

    /**
     * cria uma combobox (uma caixa de seleção) com a lista de submissiveis
     *
     * @return cmbSubmessiveis
     */
    private JComboBox criarComboBoxSubmessiveis() {
        cmbSubmessiveis = new JComboBox(controller.getSubmissiveis().toArray());
        return cmbSubmessiveis;
    }

    /**
     * cria o painel com uma lista de autores e um botao para adicionar autores
     * (esse botao irá abrir um dialogo para colocar os dados do autor
     *
     * @return p
     */
    private JPanel criarPainelAutores() {
        JPanel p = new JPanel(new BorderLayout());
        p.add(new JLabel("Autores:"), BorderLayout.NORTH);
        la = new ListaAutor();
        list = new JList(la.getLista().toArray());
        list.setModel(new ModeloListaAutores(la));
        JScrollPane scroll = new JScrollPane(list);
        JButton butaoAdicionarButton = new JButton("Adicionar Autor");
        butaoAdicionarButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                AdicionarAutor adicionarAutor = new AdicionarAutor(SubmeterArtigoUI.this, list, la);
                repaint();
            }
        });
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        p.add(scroll, BorderLayout.CENTER);
        p.add(butaoAdicionarButton, BorderLayout.SOUTH);
        return p;
    }

    /**
     * cria um painel dos campos relativos a ficheiros (anexar do ficheiro PDF
     * do artigo e a importação de ficheiros com dados da submissao (em .txt e
     * em .bin))
     *
     * @return p
     */
    private JPanel criarPainelFicheiros() {
        JPanel p = new JPanel(new GridLayout(2, 2, 10, 5));
        labelPDF = new JTextField();
        JButton butaoPDF = new JButton("Adicionar PDF");
        butaoPDF.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(".");
                definirFiltro(fc);
                int result = fc.showDialog(fc, "Attach PDF");
                if (result == JFileChooser.APPROVE_OPTION) {
                    try {
                        String path = fc.getSelectedFile().getCanonicalPath();
                        labelPDF.setText(path);
                    } catch (IOException ex) {
                        Logger.getLogger(SubmeterArtigoUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }

            private void definirFiltro(JFileChooser fc) {
                fc.setFileFilter(new FileFilter() {

                    @Override
                    public boolean accept(File f) {
                        if (f.isDirectory()) {
                            return true;
                        }
                        String extensao = extensao(f);
                        if (extensao != null) {
                            return extensao.equals("pdf");
                        }
                        return false;
                    }

                    @Override
                    public String getDescription() {
                        return "*.pdf";
                    }

                    public String extensao(File f) {
                        String extensao = null;
                        String s = f.getName();
                        int i = s.lastIndexOf('.');

                        if (i != 1) {
                            extensao = s.substring(i + 1).toLowerCase();
                        }
                        return extensao;
                    }
                });
            }
        });

        JButton btnImportArtigoBin = new JButton("Importar Submisão através de ficheiro .bin");
        btnImportArtigoBin.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Submissao sub;

                JFileChooser fc = new JFileChooser(new File(".").getAbsolutePath() + "\\data\\");
                definirFiltro(fc);
                int result = fc.showDialog(fc, "Open");
                if (result == JFileChooser.APPROVE_OPTION) {
                    File f = fc.getSelectedFile();
                    Object o = new InputOutputObjetos().lerObjeto(f.getName());

                    if (o instanceof Submissao) {
                        sub = (Submissao) o;
                        SubmeterArtigoUI.this.txtTitulo.setText(sub.getArtigo().getTitulo());
                        SubmeterArtigoUI.this.textAreaResumo.setText(sub.getArtigo().getResumo());
                        SubmeterArtigoUI.this.cmbSubmessiveis.setSelectedItem(sub.getS());

                        List<Autor> ui = sub.getArtigo().getlAut().getLista();
                        SubmeterArtigoUI.this.la = new ListaAutor(ui);
                        list.setListData(la.getLista().toArray());
                        SubmeterArtigoUI.this.list.setSelectedValue(sub.getArtigo().getAutorCorrespondente(), true);
                        repaint();
                        SubmeterArtigoUI.this.labelPDF.setText(sub.getArtigo().getPdf().getAbsolutePath());

                        SubmeterArtigoUI.this.la = sub.getArtigo().getlAut();
                    } else {
                        JOptionPane.showMessageDialog(SubmeterArtigoUI.this, "Erro a Ler o Ficheiro, verifique se o ficheiro contém dados duma submissão", "Erro de Leitura", JOptionPane.OK_OPTION);
                    }

                }
            }

            private void definirFiltro(JFileChooser fc) {
                fc.setFileFilter(new FileFilter() {

                    @Override
                    public boolean accept(File f) {
                        if (f.isDirectory()) {
                            return true;
                        }
                        String extensao = extensao(f);
                        if (extensao != null) {
                            return extensao.equals("bin");
                        }
                        return false;
                    }

                    @Override
                    public String getDescription() {
                        return "*.bin";
                    }

                    public String extensao(File f) {
                        String extensao = null;
                        String s = f.getName();
                        int i = s.lastIndexOf('.');

                        if (i != 1) {
                            extensao = s.substring(i + 1).toLowerCase();
                        }
                        return extensao;
                    }
                });
            }
        });
        JButton btnImportArtigoTexto = new JButton("Importar Submisão através de ficheiro de texto");
        btnImportArtigoTexto.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                JFileChooser fc = new JFileChooser(new File(".").getAbsolutePath() + "\\data\\");
                definirFiltro(fc);
                int result = fc.showDialog(fc, "Open");
                if (result == JFileChooser.APPROVE_OPTION) {
                    File f = fc.getSelectedFile();
                    Submissao sub = CodificadorDescodificadorSubmissaoTxt.lerTXT(f);

                    SubmeterArtigoUI.this.txtTitulo.setText(sub.getArtigo().getTitulo());
                    SubmeterArtigoUI.this.textAreaResumo.setText(sub.getArtigo().getResumo());
                    SubmeterArtigoUI.this.cmbSubmessiveis.setSelectedItem(sub.getS());
                    System.out.println(sub.getArtigo().getlAut());
                    List<Autor> ui = sub.getArtigo().getlAut().getLista();
                    SubmeterArtigoUI.this.la = new ListaAutor(ui);
                    list.setListData(la.getLista().toArray());
                    SubmeterArtigoUI.this.list.setSelectedValue(sub.getArtigo().getAutorCorrespondente(), true);
                    repaint();
                    SubmeterArtigoUI.this.labelPDF.setText(sub.getArtigo().getPdf().getAbsolutePath());

                    SubmeterArtigoUI.this.la = sub.getArtigo().getlAut();

                }
            }

            private void definirFiltro(JFileChooser fc) {
                fc.setFileFilter(new FileFilter() {

                    @Override
                    public boolean accept(File f) {
                        if (f.isDirectory()) {
                            return true;
                        }
                        String extensao = extensao(f);
                        if (extensao != null) {
                            return extensao.equals("txt");
                        }
                        return false;
                    }

                    @Override
                    public String getDescription() {
                        return "*.txt";
                    }

                    public String extensao(File f) {
                        String extensao = null;
                        String s = f.getName();
                        int i = s.lastIndexOf('.');

                        if (i != 1) {
                            extensao = s.substring(i + 1).toLowerCase();
                        }
                        return extensao;
                    }

                });
            }
        ;
        });
        
        p.add(labelPDF);
        p.add(butaoPDF);
        p.add(btnImportArtigoBin);
        p.add(btnImportArtigoTexto);
        return p;
    }

    /**
     * cria o painel com o botao para concluir a submissao e o botao para
     * cancelar a submissao
     *
     * @return p
     */
    private JPanel criarPanelButoesFormulario() {
        JPanel p = new JPanel(new FlowLayout());

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 20;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;

        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(criarBotaoOKFormulario());
        p.add(criarBotaoCancelarFormulario());

        return p;
    }

    /**
     * cria o botao que confirma a submissao faz a verificação através do email
     * se o autor correspondente (autor selecionado da lista) é utilizador do
     * sistema, caso não seja, pede para registar. guarda a submissão em .bin
     * pergunta ao utilizador se deseja guardar a submissao também em .txt
     *
     * @return btnOK
     */
    private JButton criarBotaoOKFormulario() {
        JButton btnOK = new JButton("OK");
        btnOK.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Autor au = null;
                try {
                    au = (Autor) list.getSelectedValue();
                    Utilizador autorCorrespondente = lu.findUtilizador(au.getEmail());
                    Artigo a = null;
                    if (autorCorrespondente == null) {
                        String username = JOptionPane.showInputDialog(SubmeterArtigoUI.this, "O autor correspondente não é utilizador do sistema por favor insire a username:").trim();
                        String password = null;
                        if (username != null) {
                            password = JOptionPane.showInputDialog(SubmeterArtigoUI.this, "O autor correspondente não é utilizador do sistema por favor insire a password:").trim();
                        }
                        autorCorrespondente = new Utilizador(username, au.getEmail(), password);
                        lu.add(autorCorrespondente);
                        lu.guardar();
                        a = new Artigo(txtTitulo.getText(), textAreaResumo.getText(), la, new File(labelPDF.getText()), autorCorrespondente);
                    } else {
                        a = new Artigo(txtTitulo.getText(), textAreaResumo.getText(), la, new File(labelPDF.getText()), autorCorrespondente);
                    }

                    Submissao s1 = new Submissao(a, (Submissiveis) cmbSubmessiveis.getSelectedItem());

                    String nomeFich = JOptionPane.showInputDialog("Com que nome deseja guardar a submissao?");
                    File f = new File(nomeFich + ".bin");
                    while (f.exists()) {
                        nomeFich = JOptionPane.showInputDialog("Existe uma submissão guardada com esse nome, escolha outro nome.") + ".bin";
                        f = new File(nomeFich + ".bin");
                    }
                    new InputOutputObjetos().gravarObjeto(s1, nomeFich + ".bin");
                    JOptionPane.showMessageDialog(SubmeterArtigoUI.this, "Submissão guardada com sucesso");
                    if (JOptionPane.showConfirmDialog(SubmeterArtigoUI.this, "Deseja salvar a submissao em txt", "Salvar em txt", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        CodificadorDescodificadorSubmissaoTxt.guardarTXT(s1, new File(".\\data\\" + nomeFich + ".txt"));
                    }
                    dispose();
                } catch (NullPointerException ne) {
                    JOptionPane.showMessageDialog(SubmeterArtigoUI.this, "Não foi selecionado o autor correspondente.");
                }
            }
        });
        return btnOK;
    }

    /**
     * cria o botão que cancela a submissao
     *
     * @return btnCancelar
     */
    private JButton criarBotaoCancelarFormulario() {
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btnCancelar;
    }
}
