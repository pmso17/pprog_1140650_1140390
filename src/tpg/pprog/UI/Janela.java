/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.pprog.UI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.PopupMenu;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import tpg.pprog.model.ListaAutor;

/**
 *
 * @author N550JK-CN456H
 */
public class Janela extends JFrame {

    private JButton btnSubmeter;

    /**
     * Construtor Vazio Ele inicia o frame principal com um menu e um botão para
     * submeter artigos
     *
     */
    public Janela() {
        super("Sistema de Gestão de Eventos Científicos");
        this.setLayout(new GridLayout(3, 1));

        JMenuBar menuBar = criarBarraMenus();
        setJMenuBar(menuBar);

        JPanel pPrincipal = criarPainelButoes();

        this.add(setLogo());
        this.add(pPrincipal);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                sair();
            }
        });

        this.setIcon();
        this.setSize(getWidth(), getHeight());
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    /**
     *
     * @return p painel com o botão para submeter artigos
     */
    private JPanel criarPainelButoes() {
        JPanel p = new JPanel(new FlowLayout());

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 20;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;

        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(criarBotaoSubmeter());
        return p;
    }

    /**
     *
     * @return btnSubmeter este botao ao ser pressionado abre uma janela para
     * artigos serem submetidos
     */
    private JButton criarBotaoSubmeter() {
        btnSubmeter = new JButton("Submeter Novo Artigo");
        btnSubmeter.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new SubmeterArtigoUI(Janela.this);
            }
        });
        return btnSubmeter;
    }

    /**
     * altera o icone default da janela do java para a imagem contida no
     * ficheiro imagem.png
     */
    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("imagem.png")));
    }

    /**
     * @return label com uma imagem
     */
    private JLabel setLogo() {
        ImageIcon icon = new ImageIcon(getClass().getResource("titulo.png"));
        JLabel label = new JLabel(icon);
        return label;
    }

    /**
     * cria uma barra de menu com 2 menus (menu Ficheiro e menu Ajuda)
     *
     * @return menuBar
     */
    private JMenuBar criarBarraMenus() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(criarMenuFile());
        menuBar.add(criarMenuHelp());

        return menuBar;
    }

    /**
     * cria o menu Ficheiro com o item de menu Sair
     *
     * @return menu
     */
    private JMenu criarMenuFile() {
        JMenu menu = new JMenu("Ficheiro");
        menu.add(criarItemSair());
        return menu;
    }

    /**
     * cria o item sair, que serve para terminar a aplicação (pedindo
     * confirmação ao utilizador)
     *
     * @see sair()
     * @return item
     */
    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", KeyEvent.VK_A);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sair();
            }
        });
        return item;
    }

    /**
     * cria o menu Ajuda com 2 itens de menu, ajuda e acerca
     *
     * @return menu
     */
    private JMenu criarMenuHelp() {
        JMenu menu = new JMenu("Ajuda");

        menu.setMnemonic(KeyEvent.VK_A);

        menu.add(criarItemHelp());

        menu.addSeparator();

        menu.add(criarItemAcerca());

        return menu;
    }

    /**
     * cria o item de menu acerca
     *
     * @return item
     */
    private JMenuItem criarItemAcerca() {
        JMenuItem item = new JMenuItem("Acerca", KeyEvent.VK_A);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(
                        Janela.this,
                        "Copyright © 2015 por Pedro Oliveira 1140650 e António Carvalho 1140390\n\n"
                        + "Todos os direitos reservados.",
                        "Acerca",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
        return item;
    }

    /**
     * cria o item de menu ajuda
     *
     * @return item
     */
    private JMenuItem criarItemHelp() {
        JMenuItem item = new JMenuItem("Ajuda", KeyEvent.VK_H);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(
                        Janela.this,
                        "Caso necessite de ajuda não hesite, contacte a nossa equipa.",
                        "Ajuda",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
        return item;
    }

    /**
     * confirma se o utilizador quer sair (evita que o utilizador feche sem
     * querer)
     */
    private void sair() {
        if (JOptionPane.showConfirmDialog(Janela.this, "Deseja mesmo sair?", "Sair", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            dispose();
        }
    }
}
