/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Formatter;
import java.util.IllegalFormatException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import tpg.pprog.model.Artigo;
import tpg.pprog.model.Autor;
import tpg.pprog.model.Evento;
import tpg.pprog.model.ListaAutor;
import tpg.pprog.model.Local;
import tpg.pprog.model.SessaoTematica;
import tpg.pprog.model.Submissao;
import tpg.pprog.model.Submissiveis;
import tpg.pprog.model.Utilizador;

/**
 *
 * @author i140650
 */
public class CodificadorDescodificadorSubmissaoTxt {

    /**
     * cria um ficheiro txt com as informações da submissão
     *
     * @param s submissao
     * @param f ficheiro a ser criado
     */
    public static void guardarTXT(Submissao s, File f) {
        try {
            f.createNewFile();
            Formatter out = new Formatter(f);
            out.format("%s%n", "<sub>");
            out.format("%s%n", "    <artigo>");
            out.format("%s%n", "        <titulo>");
            out.format("%s%s%n", "            ", s.getArtigo().getTitulo());
            out.format("%s%n", "        </titulo>");
            out.format("%s%n", "        <desc>");
            out.format("%s%s%n", "            ", s.getArtigo().getResumo());
            out.format("%s%n", "        </desc>");
            out.format("%s%n", "        <listaAutor>");

            for (Autor a : s.getArtigo().getlAut().getLista()) {
                out.format("%s%n", "             <autor>");
                out.format("%s%n", "                 <nome>");
                out.format("%s%n", "                     " + a.getNome());
                out.format("%s%n", "                 </nome>");
                out.format("%s%n", "                 <afili>");
                out.format("%s%n", "                     " + a.getAfiliacao());
                out.format("%s%n", "                 </afili>");
                out.format("%s%n", "                 <email>");
                out.format("%s%n", "                     " + a.getEmail());
                out.format("%s%n", "                 </email>");
                out.format("%s%n", "             </autor>");
            }

            out.format("%s%n", "        </listaAutor>");
            out.format("%s%n", "        <autorCorre>");
            out.format("%s%n", "            <user>");
            out.format("%s%n", "                    " + s.getArtigo().getAutorCorrespondente().getNome());
            out.format("%s%n", "            </user>");
            out.format("%s%n", "            <email>");
            out.format("%s%n", "                    " + s.getArtigo().getAutorCorrespondente().getEmail());
            out.format("%s%n", "            </email>");
            out.format("%s%n", "            <passwd>");
            out.format("%s%n", "                    null");
            out.format("%s%n", "            </passwd>");
            out.format("%s%n", "        </autorCorre>");
            out.format("%s%n", "        <pdf>");
            out.format("%s%n", "        " + ".\\data\\" + s.getArtigo().getPdf().getName());
            out.format("%s%n", "        </pdf>");
            out.format("%s%n", "    </artigo>");
            out.format("%s%n", "    <subme>");
            out.format("%s%n", "        <tipo>");
            out.format("%s%n", "            " + s.getS().getClass().getSimpleName());
            out.format("%s%n", "        </tipo>");
            out.format("%s%n", "        <dados>");

            if (s.getS() instanceof Evento) {
                Evento e = (Evento) s.getS();
                out.format("%s%n", "        [" + e.getTitulo() + "," + e.getDescricao() + "]");
            } else {
                if (s.getS() instanceof SessaoTematica) {
                    SessaoTematica st = (SessaoTematica) s.getS();
                    out.format("%s%n", "        [" + st.getCodigo() + "," + st.getDescricao() + "]");
                } else {
                    throw new IllegalArgumentException("Erro na criação do ficheiro.");
                }
            }

            out.format("%s%n", "        </dados>");
            out.format("%s%n", "    </subme>");
            out.format("%s%n", "</sub>");

            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CodificadorDescodificadorSubmissaoTxt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CodificadorDescodificadorSubmissaoTxt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * lê o ficheiro f e retorna uma submissao
     *
     * @param f ficheiro
     * @return submissao
     */
    public static Submissao lerTXT(File f) {
        Submissiveis sub = null;
        File pdf = null;
        ListaAutor lAu = new ListaAutor();
        Submissao s = new Submissao(new Artigo(null, null, lAu, pdf, null), sub);
        String st = null;
        String[] tempStr = new String[3];
        try {
            Scanner in = new Scanner(f);
            while (in.hasNextLine()) {
                st = in.nextLine().trim();
                if ("<artigo>".equals(st)) {
                    st = in.nextLine().trim();
                    while (!"</artigo>".equals(st)) {
                        if ("<titulo>".equals(st)) {
                            st = in.nextLine().trim();
                            s.getArtigo().setTitulo(st);
                        }
                        if ("<desc>".equals(st)) {
                            st = in.nextLine().trim();
                            s.getArtigo().setResumo(st);
                        }
                        if ("<listaAutor>".equals(st)) {
                            st = in.nextLine().trim();
                            while ("<autor>".equals(st)) {
                                while (!"</autor>".equals(st)) {
                                    if ("<nome>".equals(st)) {
                                        st = in.nextLine().trim();
                                        tempStr[0] = st;
                                    }
                                    if ("<afili>".equals(st)) {
                                        st = in.nextLine().trim();
                                        tempStr[1] = st;
                                    }
                                    if ("<email>".equals(st)) {
                                        st = in.nextLine().trim();
                                        tempStr[2] = st;
                                    }

                                    st = in.nextLine().trim();
                                }
                            }
                            if ("</autor>".equals(st)) {
                                st = in.nextLine().trim();
                                lAu.adicionarAutor(new Autor(tempStr[0], tempStr[1], tempStr[2]));
                                st = in.nextLine().trim();
                            }
                            s.getArtigo().setlAut(lAu);
                        }
                        while ("<autorCorre>".equals(st)) {
                            while (!"</autorCorre>".equals(st)) {
                                Utilizador u = null;
                                if ("<user>".equals(st)) {
                                    st = in.nextLine().trim();
                                    tempStr[0] = st;
                                }
                                if ("<email>".equals(st)) {
                                    st = in.nextLine().trim();
                                    tempStr[1] = st;
                                }
                                if ("<passwd>".equals(st)) {
                                    st = in.nextLine().trim();
                                    tempStr[2] = st;

                                }
                                if ("</autorCorre>".equals(st)) {
                                    st = in.nextLine().trim();
                                    s.getArtigo().setAutorCorrespondente(new Utilizador(tempStr[0], tempStr[1], tempStr[2]));
                                }
                                st = in.nextLine().trim();
                            }
//                            st = in.nextLine().trim();
                        }
                        if ("<pdf>".equals(st)) {
                            st = in.nextLine().trim();
                            s.getArtigo().setPdf(new File(st));
                        }
                        st = in.nextLine().trim();
                    }
                    st = in.nextLine().trim();
                }
                while ("<subme>".equals(st)) {
                    st = in.nextLine().trim();
                    if ("<tipo>".equals(st)) {
                        st = in.nextLine().trim();
                        if ("Evento".equals(st)) {
                            st = in.nextLine().trim();
                            st = in.nextLine().trim();
                            if ("<dados>".equals(st)) {
                                st = in.nextLine().trim();
                                st = st.replace("[", "");
                                tempStr = st.trim().split(",");
                                sub = new Evento(tempStr[0], tempStr[1], null, null, "Rua Fake Street");
                            }
                        } else {
                            if ("SessaoTematica".equals(st) || "Sessao Tematica".equals(st) || "Sessao Temática".equals(st)) {
                                st = in.nextLine().trim();
                                st = in.nextLine().trim();
                                st = st.replace("[", "");
                                tempStr = st.trim().split(",");
                                sub = new SessaoTematica(Integer.parseInt(tempStr[0]), tempStr[1], null);
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CodificadorDescodificadorSubmissaoTxt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;
    }
}
