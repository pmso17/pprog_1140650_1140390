/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpg.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author N550JK-CN456H
 */
public class InputOutputObjetos {
/**
 * grava o objeto o num ficheiro com o nome especificado
 * @param o objeto a ser guardado
 * @param nome nome do ficheiro em que é guardado o objeto
 */
    public void gravarObjeto(Object o, String nome) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path()+"\\data\\"+nome);
            ObjectOutputStream outStream = new ObjectOutputStream(fileOut);
            outStream.writeObject(o);
            outStream.close();
            fileOut.close();
            System.out.println(nome+" salvo");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
/**
 * le o ficheiro binario e retira as informações do ficheiro de forma a criar o objeto o
 * @param nome do ficheiro que tem as informações do objeto o
 * @return o objeto lido
 */
    public Object lerObjeto(String nome) {
        Object o=null;
        try {
            FileInputStream fileIn = new FileInputStream(path()+"\\data\\"+nome);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            o = in.readObject();
            in.close();
            fileIn.close();
            System.out.println(nome+" lido");
        } catch (IOException i) {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }catch(Exception g){
            System.out.println("Erro Durante a leitura");
        }
        return o;
    }
    
    private String path(){
        try {
            return new File(".").getCanonicalPath();
        } catch (IOException ex) {
            System.out.println("Ocorreu uma exceção de Input/OutPut\n"+ex.getMessage());
        }
        return null;
    }
}
